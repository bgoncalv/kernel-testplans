<!-- markdownlint-disable-next-line MD041 -->
## kpatch/kpatch-t1

Kpatch userspace package gating tests

- enabled: `true`
- provision:
  - how: artemis
- discover:
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests.git>
  - test:
    - /general/kpatch/service

## kpatch/kpatch-t2

Kpatch Tier2 test plan

- enabled: `true`
- provision:
  - how: artemis
- discover:
  - url: <https://pkgs.devel.redhat.com/git/tests/distribution>
  - test:
    - /install/rhel-buildroot
    - /install/source-repositories
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests.git>
  - test:
    - /general/kpatch/build
    - /general/kpatch/cmdline
    - /general/kpatch/compat
    - /general/kpatch/patch-upgrade
    - /general/kpatch/stress

## kpatch/livepatch-t1

Livepatch Tier1 test plan

- enabled: `true`
- provision:
  - how: artemis
- discover:
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests.git>
  - test:
    - /livepatch/selftests
    - /livepatch/late-patching
    - /livepatch/sysfs
    - /livepatch/stress-klp
