<!-- markdownlint-disable-next-line MD041 -->
## kernel-tier2

Upstream kernel tier2 tests

- enabled: `true`
- provision:
  - how: virtual
- discover:
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests>
  - filter: `tier: 2&component: kernel`
  - url: <https://gitlab.com/rh-kernel-stqe/sts>
  - filter: `tier: 2`

More test plans can be found at:

- [hardware/userspace](documentation/hardware/userspace.md)
- [kernel-tier1](documentation/kernel-tier1.md)
- [kpatch](documentation/kpatch.md)
- [podman](documentation/podman.md)
- [security](documentation/security.md)
- [storage/iscsi](documentation/storage/iscsi.md)
- [storage/iscsi/offload](documentation/storage/iscsi/offload.md)
- [storage/lvm](documentation/storage/lvm.md)
- [storage/ssd](documentation/storage/ssd.md)
- [storage/stratis](documentation/storage/stratis.md)
- [storage/vdo](documentation/storage/vdo.md)
