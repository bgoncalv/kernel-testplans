<!-- markdownlint-disable-next-line MD041 -->
## storage/stratis/remote

Stratis test plan

- enabled: `true`
- provision:
  - how: connect
- discover:
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests>
  - test:
    - /storage/stratis/common
