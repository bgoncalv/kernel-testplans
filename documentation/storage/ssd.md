<!-- markdownlint-disable-next-line MD041 -->
## storage/ssd/artemis

nvme ssd tests test plan

- enabled: `true`
- provision:
  - how: artemis
  - hardware:
`{
    "hostname": "storageqe-62.rhts.eng.pek2.redhat.com"
}`
- discover:
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests>
  - exclude:
    - /storage/ssd/nvme_blktests
    - /storage/ssd/nvme_cli_command_test
    - /storage/ssd/nvme_controller_reset_goes_offline_bz1334462
    - /storage/ssd/nvme_cpu_random_online_offline_io_bz1376948
    - /storage/ssd/nvme_format_command_test_bz1449838
    - /storage/ssd/nvme_mkfs_xfs_bz1443807
    - /storage/ssd/nvme_mq_io_stress_testing
    - /storage/ssd/nvme_setting_nr_request_bz1397927
    - /storage/ssd/nvme_sg_inq_scsi_vpd_info_bz1749524
    - /storage/ssd/nvme_xfs_50G_file_create_bz1227342
    - /storage/ssd/nvme_mq_io_scheduler_testing
    - /storage/ssd/nvme_module_rmmod_insmod
    - /storage/ssd/nvme_format_with_lbaf_1_bz1746946
    - /storage/ssd/nvme_pci_adapter_rescan_reset_remove_bz1370507
    - /storage/ssd/nvme_removal_cause_stuck_process_bz1279699
  - test:
    - /storage/ssd/test_devs_setup
    - /storage/ssd/nvme_io_stress_testing

## storage/ssd/beaker

nvme ssd tests test plan

- enabled: `true`
- provision:
  - how: beaker
  - hardware:
`{
    "hostname": "storageqe-62.rhts.eng.pek2.redhat.com"
}`
- discover:
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests>
  - exclude:
    - /storage/ssd/nvme_blktests
    - /storage/ssd/nvme_cli_command_test
    - /storage/ssd/nvme_controller_reset_goes_offline_bz1334462
    - /storage/ssd/nvme_cpu_random_online_offline_io_bz1376948
    - /storage/ssd/nvme_format_command_test_bz1449838
    - /storage/ssd/nvme_mkfs_xfs_bz1443807
    - /storage/ssd/nvme_mq_io_stress_testing
    - /storage/ssd/nvme_setting_nr_request_bz1397927
    - /storage/ssd/nvme_sg_inq_scsi_vpd_info_bz1749524
    - /storage/ssd/nvme_xfs_50G_file_create_bz1227342
    - /storage/ssd/nvme_mq_io_scheduler_testing
    - /storage/ssd/nvme_module_rmmod_insmod
    - /storage/ssd/nvme_format_with_lbaf_1_bz1746946
    - /storage/ssd/nvme_pci_adapter_rescan_reset_remove_bz1370507
    - /storage/ssd/nvme_removal_cause_stuck_process_bz1279699
  - test:
    - /storage/ssd/test_devs_setup
    - /storage/ssd/nvme_io_stress_testing

## storage/ssd/local

nvme ssd tests

- enabled: `true`
- provision:
  - how: local
- discover:
  - test:
    - /storage/ssd/test_devs_setup
    - /storage/ssd/nvme_io_stress_testing

## storage/ssd/remote-testrun

nvme ssd tests test plan

- enabled: `true`
- provision:
  - how: connect
- discover:
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests>
  - exclude:
    - /storage/ssd/nvme_format_command_test_bz1449838
    - /storage/ssd/nvme_format_with_lbaf_1_bz1746946
    - /storage/ssd/nvme_pci_adapter_rescan_reset_remove_bz1370507
    - /storage/ssd/nvme_mkfs_xfs_bz1443807
    - /storage/ssd/nvme_setting_nr_request_bz1397927
    - /storage/ssd/nvme_sg_inq_scsi_vpd_info_bz1749524
    - /storage/ssd/nvme_xfs_50G_file_create_bz1227342
    - /storage/ssd/nvme_cpu_random_online_offline_io_bz1376948
    - /storage/ssd/nvme_controller_reset_goes_offline_bz1334462
    - /storage/ssd/nvme_io_stress_testing
    - /storage/ssd/nvme_module_rmmod_insmod
    - /storage/ssd/nvme_mq_io_stress_testing
    - /storage/ssd/nvme_mq_io_scheduler_testing
    - /storage/ssd/nvme_removal_cause_stuck_process_bz1279699
  - test:
    - /storage/ssd/test_devs_setup
    - /storage/ssd/nvme_cli_command_test
