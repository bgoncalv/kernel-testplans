<!-- markdownlint-disable-next-line MD041 -->
## security/audit

Audit security test suite

- enabled: `true`
- provision:
  - how: artemis
- discover:
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests.git>
  - test:
    - /security/audit/2028871
    - /security/audit/2035123
    - /security/audit/testsuite

## security/crypto

Crypto security test suite

- enabled: `true`
- provision:
  - how: artemis
- discover:
  - url: <https://pkgs.devel.redhat.com/git/tests/distribution>
  - test:
    - /install/rhel-buildroot
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests.git>
  - test:
    - /security/crypto/1528343
    - /security/crypto/1600394
    - /security/crypto/1693350
    - /security/crypto/1990040
    - /security/crypto/2174928
    - /security/crypto/2229643
    - /security/crypto/17113
    - /security/crypto/ltp
    - /security/crypto/enable_fips
    - /security/crypto/rhel-24869

## security/ima-evm-utils-t1

ima-evm-utils userspace package gating tests

- enabled: `true`
- provision:
  - how: artemis
- discover:
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests.git>
  - test:
    - /security/integrity/ima-evm-utils/sanity

## security/integrity

Integrity security test suite

- enabled: `true`
- provision:
  - how: artemis
- discover:
  - url: <https://pkgs.devel.redhat.com/git/tests/distribution>
  - test:
    - /install/rhel-buildroot
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests.git>
  - test:
    - /security/integrity/config_options
    - /security/integrity/2063913
    - /security/integrity/1934949
    - /security/integrity/1925661
    - /security/integrity/1907556
    - /security/integrity/1847219
    - /security/integrity/1843774
    - /security/integrity/1662947
    - /security/integrity/1896046
    - /security/integrity/ima-evm-utils/sanity
    - /security/key
    - /security/integrity/ltp

## security/regression-upstream

Security regression test suite

- enabled: `true`
- provision:
  - how: artemis
- discover:
  - url: <https://pkgs.devel.redhat.com/git/tests/distribution>
  - test:
    - /install/rhel-buildroot
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests.git>
  - test:
    - /security/vulnerabilities
    - /security/rpm-verify
    - /security/krsi
    - /security/ltp

## security/selinux

SELinux test suite

- enabled: `true`
- provision:
  - how: artemis
- discover:
  - url: <https://pkgs.devel.redhat.com/git/tests/distribution>
  - test:
    - /install/rhel-buildroot
  - url: <https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests.git>
  - test:
    - /security/selinux/testsuite
